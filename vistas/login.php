<?php
session_start();
if(isset($_SESSION['user_id'])) {
    header('Location: index.php');
}
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--    favicon-->
    <link rel="icon" type="image/png" href="../img/school.png">
    <!--    Titulo-->
    <title>Inicio de sesion</title>
    <!--    Bootstrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"
            crossorigin="anonymous"></script>
    <!--    CSS local-->
    <link href="../css/form.css" rel="stylesheet">
</head>
<body class="text-center">
<main class="form-signin">
    <form method="post" action="../procesos/loginProceso.php">
        <img src="../img/mundo.jpg" alt="logo mundo" class="mb-4" width="72" height="72">
        <h1 class="h3 mb-3 fw-normal">¡Bienvenido!</h1>
        <div class="form-floating">
            <input type="text" class="form-control" id="nombre_de_usuario" name="username" placeholder="name@example.com">
            <label for="nombre_de_usuario">Nombre de usuario</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            <label for="password">Contraseña</label>
        </div>
        <button type="submit" class="btn btn-primary" value="Login">Iniciar</button>
        <a href="signup.php" class="btn btn-secondary" role="button" aria-disabled="true">Registrarse</a>
    </form>
</main>
</body>
</html>