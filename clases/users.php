<?php
require_once("functions.php");

class User
{
    private $username;
    private $user_id;
    private $password;

    public function __construct()
    {
        $this->username = "";
        $this->password = "";
        $this->user_id = "";
    }

    public function registrarse($username, $password, $con)
    {
        $this->setUsername($username);
        $this->setPassword($password);
        $this->setUserId(20);

        $query = "INSERT INTO users (user_id,user_name,password) VALUES ('$this->user_id','$this->username','$this->password')";
        return mysqli_query($con, $query);
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param string $len
     */
    public function setUserId($len)
    {
        $this->user_id = random_num($len);
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }
}