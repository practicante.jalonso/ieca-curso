<?php

$dbhost = "localhost";
$dbuser = "root";
$dbpass = "";
$dbname = "proyecto_login";

try {
    $db = mysqli_connect($dbhost,$dbuser,$dbpass,$dbname);
} catch (\mysqli_sql_exception $e) {
    throw new \mysqli_sql_exception($e->getMessage(), $e->getCode());
}

unset($dbhost);
unset($dbuser);
unset($dbpass);
unset($dbname);