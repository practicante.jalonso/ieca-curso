<?php
/**
 * @var PDO $db
 */
session_start();
include("models/connection.php");
include("procesos/functions.php");

$user_data = check_login($db);
$alumnos = obtener_alumnos($db);
?>

<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--    favicon-->
    <link rel="icon" type="image/png" href="img/school.png">
    <!--    Titulo-->
    <title>Bienvenido!</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"
            crossorigin="anonymous"></script>
</head>
<body>

<!--Header-->
<div class="container">
    <header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between py-3 mb-4 border-bottom">
        <h1>Hola, <?php echo $user_data['user_name']; ?></h1>

        <div class="col-md-3 text-end">
            <button type="button" class="btn btn-outline-primary me-2"><a href="procesos/logout.php">Logout</a></button>
        </div>
    </header>
</div>

<!--Título-->
<div class="container">
    <h5>Un gusto tenerle de vuelta</h5>
</div>

<!--Tabla de los alumnos-->
<div class="container">
    <h3>Reportes de Estudiantes</h3>
    <table>
        <tr>
            <td>Código</td>
            <td>Apelidos</td>
            <td>Nombres</td>
            <td>Parcial</td>
            <td>Final</td>
            <td>Promedio</td>
            <td>Editar</td>
            <td>Eliminar</td>
        </tr>
        <?php
        foreach ($alumnos as $alumno) {
            ?>
            <tr>
                <td><?php echo $alumno['id_alumno']; ?></td>
                <td><?php echo $alumno['a_paterno'] . ' ' . $alumno['a_materno']; ?></td>
                <td><?php echo $alumno['nombre']; ?></td>
                <td><?php echo $alumno['ex_parcial']; ?></td>
                <td><?php echo $alumno['ex_final']; ?></td>
                <td><?php echo ($alumno['ex_parcial'] + $alumno['ex_final']) / 2; ?></td>
                <td><a href="procesos/editar.php?id=<?php echo $alumno['id_alumno']; ?>">✏️</a></td>
                <td><a href="procesos/eliminar.php?id=<?php echo $alumno['id_alumno']; ?>">⛔</a></td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>

<!--Insertar un nuevo registro-->
<div class="container">
    <h3>
        Registrar un estudiante nuevo:
    </h3>
    <form action="procesos/insert.php" method="post">
        <table>
            <tr>
                <td><label>Apellido paterno
                        <input type="text" name="txtPat">
                    </label></td>
            </tr>
            <tr>
                <td><label>Apellido materno
                        <input type="text" name="txtMat">
                    </label></td>
            </tr>
            <tr>
                <td><label>Nombre(s)
                        <input type="text" name="txtNom">
                    </label></td>
            </tr>
            <tr>
                <td><label>Calificación parcial
                        <input type="text" name="txtPar">
                    </label></td>
            </tr>
            <tr>
                <td><label>Calificación final
                        <input type="text" name="txtFin">
                    </label></td>
            </tr>
            <input type="hidden" name="oculto" value="1">
            <tr>
                <td><input type="reset" name=""></td>
                <td><input type="submit" value="Register"></td>
            </tr>
        </table>
    </form>
</div>

</body>
</html>