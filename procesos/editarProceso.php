<?php
/**
 * @var PDO $db
 */
session_start();

if (!isset($_SESSION['nombre'])) {
    header('Location: login.php');
}
if (!isset($_POST['id_alumno'])) {
    header('Location: index.php');
}

include '../models/connection.php';

$id_alumno = $_POST['id_alumno'];
$a_paterno = $_POST['newTxtFathers'];
$a_materno = $_POST['newTxtMothers'];
$nombre = $_POST['newTxtName'];
$ex_parcial = $_POST['newTxtMidterm'];
$ex_final = $_POST['newTxtFinal'];

try {
    $query = "UPDATE alumnos SET a_paterno = '$a_paterno', a_materno = '$a_materno', nombre = '$nombre', ex_parcial = '$ex_parcial',
                  ex_final = '$ex_final' WHERE id_alumno = '$id_alumno'";
    $result = mysqli_query($db, $query);
//    $statement = $db->prepare("UPDATE alumnos SET a_paterno = ?, a_materno = ?, nombre = ?, ex_parcial = ?,
//                  ex_final = ? WHERE id_alumno = ?");
//    $statement->execute([$a_paterno, $a_materno, $nombre, $ex_parcial, $ex_final, $id_alumno]);
    header('Location: ../index.php');
} catch (Exception $e) {
    echo "Error de conexion " . $e->getMessage();
}