<?php
/**
 * @var PDO $db
 */
session_start();

if (!isset($_SESSION['nombre'])) {
    header('Location: login.php');
}
if (!isset($_GET['id'])) {
    die;
}

include('../models/connection.php');

$id = $_GET['id'];


try {
    $query = "DELETE FROM alumnos WHERE id_alumno = '$id'";
    mysqli_query($db, $query);
    header('Location: ../index.php');
} catch (Exception $e) {
    echo "Error de conexion " . $e->getMessage();
}