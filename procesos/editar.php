<?php
/**
 * @var PDO $db
 */
session_start();

if (!isset($_SESSION['nombre'])) {
    header('Location: login.php');
}
if (!isset($_GET['id'])) {
    header('Location: index.php');
}

include('../models/connection.php');
$id = $_GET['id'];

$persona = '';
try {
    $query = "SELECT * FROM alumnos WHERE id_alumno = '$id';";
    $result = mysqli_query($db, $query);
    $persona = mysqli_fetch_row($result);
} catch (Exception $exception) {
    echo "Error de conexion " . $exception->getMessage();
}

//$sentencia = $db->prepare('SELECT * FROM alumnos WHERE id_alumno = ?;');
//$sentencia->execute([$id]);
//$persona = $sentencia->fetch(PDO::FETCH_OBJ);
?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--    favicon-->
    <link rel="icon" type="image/png" href="../img/school.png">
    <!--    Título-->
    <title>Editar Estudiante</title>
    <!--    Bootstrap-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ"
            crossorigin="anonymous"></script>
</head>
<body>

<div class="container">
    <h3>
        Editar Estudiante
    </h3>
</div>

<div class="container">

    <form method="POST" action="editarProceso.php?">
        <table>
            <tr>
                <td><label>Fathers last name
                        <input type="text" name="newTxtFathers" value="<?php echo $persona[1]; ?>">
                    </label>
                </td>
            </tr>
            <tr>
                <td><label>Mothers last name
                        <input type="text" name="newTxtMothers" value="<?php echo $persona[2]; ?>">
                    </label></td>
            </tr>
            <tr>
                <td><label>Full name
                        <input type="text" name="newTxtName" value="<?php echo $persona[3]; ?>">
                    </label>
                </td>
            </tr>
            <tr>
                <td><label>Midterm result
                        <input type="text" name="newTxtMidterm" value="<?php echo $persona[4]; ?>">
                    </label>
                </td>
            </tr>
            <tr>
                <td><label>Final result
                        <input type="text" name="newTxtFinal" value="<?php echo $persona[5]; ?>">
                    </label>
                </td>
            </tr>
            <tr>
                <input type="hidden" name="id_alumno" value="<?php echo $persona[0]; ?>">
                <td><input type="submit" value="Apply"></td>
            </tr>
        </table>
    </form>
</div>
</body>
</html>
