<?php
/**
 * @var PDO $db
 */
session_start();
include("../models/connection.php");


if ($_SERVER['REQUEST_METHOD'] == "POST") {
    //Se intento un login
    $username = $_POST['username'];
    $password = $_POST['password'];

    //Preguntamos si se lleno correctamente el form
    $existe_username = !empty($username) && !is_numeric($username);
    $existe_password = !empty($password);

    if (!$existe_username) {
        echo "Ingresa un nombre de usuario\n";
        die;
    } elseif (!$existe_password) {
        echo "Ingresa una contraseña\n";
        die;
    }
    //Si los dos existen, procedemos a revisar la base de datos
    try {
        $query = "SELECT * FROM users WHERE user_name = '$username' LIMIT 1";
        $result = mysqli_query($db, $query);
    } catch (Exception $exception) {
        echo "Error " . $exception->getMessage();
        die;
    }

    //Si tenemos un resultado, probamos la contrase;a.
    //En caso contrario indicamos que el usuario no existe.
    if ($result && mysqli_num_rows($result) > 0) {
        $user_data = mysqli_fetch_assoc($result);
        if ($user_data['password'] === $password) {
            $_SESSION['user_id'] = $user_data['user_id'];
//            echo 'Exito login ' . $_SESSION['user_id'];
            header("Location: ../index.php");
        } else {
            echo "Contraseña incorrecta";
        }
    } else {
        echo "Este usuario no esta registrado\n";
    }
    die;
}
