<?php

function obtener_alumnos($db) {
    if (isset($_SESSION['user_id'])) {
        $query = "SELECT * FROM alumnos";
        $result = mysqli_query($db, $query);
        $alumnos = [];
        if ($result && mysqli_num_rows($result) > 0) {
            $alumnos = mysqli_fetch_all($result, MYSQLI_ASSOC);
        }
        return $alumnos;
    }

    //Redirigir si el usuario no existe/esta definido
    header("Location: vistas/login.php");
    die;
}

function check_login($db)
{
    if (isset($_SESSION['user_id'])) {
        $id = $_SESSION['user_id'];
        $query = "SELECT * FROM users WHERE user_id = '$id' LIMIT 1";
        $result = mysqli_query($db, $query);
        if ($result && mysqli_num_rows($result) > 0) {
            return mysqli_fetch_assoc($result);
        }
    }

    //Redirigir si el usuario no existe/esta definido
    header("Location: vistas/login.php");
    die;
}

function random_num($length)
{
    $text = "";
    if ($length < 5) {
        $length = 5;
    }

    $len = rand(4, $length);

    for ($i = 0; $i < $len; $i++) {
        $text .= rand(0, 9);
    }

    return $text;
}