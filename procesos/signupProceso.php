<?php
/**
 * @var PDO $db
 */
session_start();
include("../models/connection.php");
include("functions.php");
include("../clases/users.php");

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    //Se intento un registro
    $username = $_POST['username'];
    $password = $_POST['password'];

    //Preguntamos si se lleno correctamente el form
    $existe_username = !empty($username) && !is_numeric($username);
    $existe_password = !empty($password);

    if (!$existe_username) {
        echo "Ingresa un nombre de usuario\n";
        die;
    }
    if (!$existe_password) {
        echo "Ingresa una contraseña\n";
        die;
    }

    //Se llega a este punto solo si ambos existen, creamos un objeto
    //para el usuario nuevo, e intentamos registrarlo
    try {
        $user = new User();
        $result = $user->registrarse($username, $password, $db);
        header("Location: ../vistas/login.php");
    } catch (Exception $exception) {
        echo "Ocurrio un error al registrarse! pruebe de nuevo";
        die;
    }
}