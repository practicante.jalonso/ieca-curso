<?php
/**
 * @var PDO $db
 */
session_start();

if (!isset($_SESSION['nombre'])) {
    header('Location: login.php');
}
if (!isset($_POST['oculto'])) {
    die;
}

include '../models/connection.php';

$a_paterno = $_POST['txtPat'];
$a_materno = $_POST['txtMat'];
$nombre = $_POST['txtNom'];
$ex_parcial = $_POST['txtPar'];
$ex_final = $_POST['txtFin'];

try {
    $query = "INSERT INTO alumnos (a_paterno, a_materno, nombre, ex_parcial, ex_final) VALUES ('$a_paterno','$a_materno','$nombre','$ex_parcial','$ex_final')";
    mysqli_query($db, $query);
    header('Location: ../index.php');
} catch (Exception $e) {
    echo "Error de conexion " . $e->getMessage();
}